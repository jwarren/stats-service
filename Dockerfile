FROM maven:3.5-jdk-11-slim as build

# Resolve all the dependencies and cache them to save a LOT of time
COPY pom.xml /usr/src/myapp/
COPY application.properties /usr/src/myapp/
RUN mvn -f /usr/src/myapp/pom.xml dependency:resolve dependency:resolve-plugins

# Build the application - usually this is the only part that gets rebuilt locally - use offline mode and skip tests
COPY src /usr/src/myapp/src
RUN mvn -f /usr/src/myapp/pom.xml clean package -DskipTests -o


# The final image should have a minimal number of layers
FROM openjdk:8u212-b04-jdk-stretch

VOLUME /tmp

COPY  --from=build /usr/src/myapp/target/*.jar /apps/
COPY docker-scripts/start.sh /

RUN ["chmod", "+x", "/start.sh"]

ENTRYPOINT ["/start.sh"]