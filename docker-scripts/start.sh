#!/bin/bash
set -e
[[ -z "${SPRING_EXPORTER_PORT}" ]] && port=9999 || port="${SPRING_EXPORTER_PORT}"
#[[ -z "${DATASOURCE_URL}" ]] && datasource='http://localhost/prometheus' || datasource="${DATASOURCE_URL}"
#java -Djava.security.egd=file:/dev/./urandom -jar /apps/stats-rest-service-0.0.1-exec.jar --server.port="${port}" --datasource.url="${datasource}"
echo "launching app"
java -cp "/apps/stats-rest-service-0.0.1-exec.jar" -Dloader.main=uk.ac.ebi.phenotype.stats.dao.StatisticsRepositoryApplication org.springframework.boot.loader.PropertiesLauncher --spring.data.mongodb.uri=mongodb://migro:read3rmig@mongodb-hx-impc-dev-001:27017,mongodb-hh-impc-dev-002:27017/admin?replicaSet=impcdevrs012 --spring.data.mongodb.database=impc --server.port=8091
# &
#echo $! > ./pid.file